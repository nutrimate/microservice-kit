package reqctx_test

import (
	"net/http/httptest"
	"testing"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
	"gitlab.com/nutrimate/microservice-kit/pkg/reqctx"
)

func TestGetToken(t *testing.T) {
	e := echo.New()

	t.Run("should return token if request had token", func(t *testing.T) {
		token := "test.jwt.token"
		c := e.NewContext(httptest.NewRequest("GET", "/", nil), httptest.NewRecorder())
		c.Set(jwksmiddleware.JWTContextKey, &jwt.Token{Raw: token})

		ctx := reqctx.Create(c)

		result := reqctx.GetToken(ctx)

		assert.Equal(t, &token, result)
	})

	t.Run("should return nil if request had token that was not a jwt.Token", func(t *testing.T) {
		c := e.NewContext(httptest.NewRequest("GET", "/", nil), httptest.NewRecorder())
		c.Set(jwksmiddleware.JWTContextKey, time.Time{})

		ctx := reqctx.Create(c)

		result := reqctx.GetToken(ctx)

		assert.Nil(t, result)
	})

	t.Run("should return nil if request did not have a token", func(t *testing.T) {
		c := e.NewContext(httptest.NewRequest("GET", "/", nil), httptest.NewRecorder())

		ctx := reqctx.Create(c)

		result := reqctx.GetToken(ctx)

		assert.Nil(t, result)
	})
}
