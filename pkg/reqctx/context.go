package reqctx

import (
	"context"

	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
)

// RequestContextKey is a type for all context keys used by this package
type RequestContextKey string

// RawJWTContextKey is a context key under which raw JWT is stored
var RawJWTContextKey RequestContextKey = "gitlab.com/nutrimate/microservice-kit/pkg/auth/RawJWT"

// Create returns request's context enriched with information about request's user
func Create(c echo.Context) context.Context {
	ctx := c.Request().Context()

	token := jwksmiddleware.GetRequestJWT(c)
	if token != nil {
		ctx = context.WithValue(ctx, RawJWTContextKey, &token.Raw)
	}

	return ctx
}

// GetToken returns raw JWT string associated with request's context
func GetToken(ctx context.Context) *string {
	value, ok := ctx.Value(RawJWTContextKey).(*string)
	if !ok {
		return nil
	}

	return value
}
