package auth_test

import (
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
)

func TestTokenClaims(t *testing.T) {
	tests := []struct {
		name              string
		token             *jwt.Token
		expectedAudiences []string
	}{
		{
			name: "should parse token with single audience as string",
			token: jwt.NewWithClaims(jwt.SigningMethodNone, jwt.MapClaims{
				"aud": "a",
			}),
			expectedAudiences: []string{"a"},
		},
		{
			name: "should parse token with array of audiences",
			token: jwt.NewWithClaims(jwt.SigningMethodNone, jwt.MapClaims{
				"aud": []string{"a", "b"},
			}),
			expectedAudiences: []string{"a", "b"},
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			tokenString, err := testCase.token.SignedString(jwt.UnsafeAllowNoneSignatureType)
			if !assert.NoError(t, err) {
				return
			}
			parser := jwt.Parser{
				ValidMethods: []string{jwt.SigningMethodNone.Alg()},
			}
			claims := &auth.TokenClaims{}

			// When
			_, _, err = parser.ParseUnverified(tokenString, claims)

			// Then
			assert.NoError(t, err)
			assert.Equal(t, testCase.expectedAudiences, []string(claims.Audience))
		})
	}
}
