package auth_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
)

func TestNewPermissionsMiddleware(t *testing.T) {
	requiredPermissions := []string{"a", "b"}
	tests := []struct {
		name           string
		token          *jwt.Token
		expectedStatus int
	}{
		{
			name: "should allow user with all of required permissions",
			token: &jwt.Token{
				Claims: &auth.TokenClaims{
					Permissions: []string{"a", "b", "c"},
				},
			},
			expectedStatus: http.StatusOK,
		},
		{
			name: "should not allow user with not all of required permissions",
			token: &jwt.Token{
				Claims: &auth.TokenClaims{
					Permissions: []string{"a", "c"},
				},
			},
			expectedStatus: http.StatusForbidden,
		},
		{
			name:           "should not allow when no user is present",
			token:          nil,
			expectedStatus: http.StatusForbidden,
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			token := testCase.token
			expectedStatus := testCase.expectedStatus
			req := httptest.NewRequest("GET", "/", nil)
			rec := httptest.NewRecorder()
			e := echo.New()
			e.Use(mockUserTokenMiddleware(token))
			e.Use(auth.NewPermissionsMiddleware(auth.Permissions{
				All: requiredPermissions,
			}))
			e.GET("/", func(c echo.Context) error {
				return c.NoContent(http.StatusOK)
			})

			// When
			e.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, rec.Code, expectedStatus)
		})
	}
}
