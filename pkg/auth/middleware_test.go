package auth_test

import (
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"strings"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
	"gitlab.com/nutrimate/microservice-kit/tests/jwkstest"
)

func mockUserTokenMiddleware(token *jwt.Token) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set(jwksmiddleware.JWTContextKey, token)
			return next(c)
		}
	}
}

func TestNewMiddleware(t *testing.T) {
	// Given
	privateKeyPath := "../../tests/jwkstest/key"
	jwksPath, err := filepath.Abs("../../tests/jwkstest/jwks.json")
	assert.NoError(t, err)

	token, err := jwkstest.CreateToken()
	assert.NoError(t, err)

	err = token.Set("sub", "1")
	assert.NoError(t, err)
	err = token.Set("permissions", []string{"a", "b"})
	assert.NoError(t, err)
	err = token.Set("scope", "email openid")
	assert.NoError(t, err)

	signedToken, err := jwkstest.SignTokenWithDefaultKey(privateKeyPath, token)
	assert.NoError(t, err)

	e := echo.New()
	e.Use(auth.NewMiddleware(auth.MiddlewareConfig{
		JWKSEndpoint: jwksPath,
		KeySetProvider: func(uri string) (jwk.Set, error) {
			return jwk.ReadFile(uri)
		},
	}))
	e.GET("/", func(c echo.Context) error {
		u := auth.GetRequestUser(c)
		return c.JSON(http.StatusOK, u)
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Authorization", "Bearer "+string(signedToken))

	// When
	e.ServeHTTP(rec, req)

	// Then
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, `{"ID":"1","Permissions":["a","b"],"Scope":"email openid"}`, strings.TrimSpace(rec.Body.String()))
}

func TestGetRequestUser(t *testing.T) {
	tests := []struct {
		name     string
		token    *jwt.Token
		expected *auth.RequestUser
	}{
		{
			name:     "should return nil when token is not present",
			token:    nil,
			expected: nil,
		},
		{
			name: "should return nil when claims are nil",
			token: &jwt.Token{
				Claims: nil,
			},
			expected: nil,
		},
		{
			name: "should return user mapped from claims",
			token: &jwt.Token{
				Claims: &auth.TokenClaims{
					Scope:       "email openid",
					Permissions: []string{"a", "b"},
					StandardClaims: jwt.StandardClaims{
						Subject: "test-1234",
					},
				},
			},
			expected: &auth.RequestUser{
				ID:          "test-1234",
				Permissions: []string{"a", "b"},
				Scope:       "email openid",
			},
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			req := httptest.NewRequest("GET", "/", nil)
			rec := httptest.NewRecorder()
			token := testCase.token
			expectedUser := testCase.expected
			var actualUser *auth.RequestUser

			e := echo.New()
			e.Use(mockUserTokenMiddleware(token))
			e.GET("/", func(c echo.Context) error {
				actualUser = auth.GetRequestUser(c)
				return c.NoContent(http.StatusOK)
			})

			// When
			e.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, rec.Code, http.StatusOK)
			assert.Equal(t, actualUser, expectedUser)
		})
	}
}
