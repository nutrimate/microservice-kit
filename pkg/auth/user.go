package auth

// RequestUser is a representation of user associated with request extract from token claims
type RequestUser struct {
	ID          string
	Permissions []string
	Scope       string
}

// HasPermission checks if user has provided permission
func (u RequestUser) HasPermission(permission string) bool {
	for _, ownPermission := range u.Permissions {
		if ownPermission == permission {
			return true
		}
	}

	return false
}

// HasAllPermissions checks if user has all provided permissions
func (u RequestUser) HasAllPermissions(required []string) bool {
	for _, requiredPermission := range required {
		if !u.HasPermission(requiredPermission) {
			return false
		}
	}

	return true
}
