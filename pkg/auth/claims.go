package auth

import (
	"encoding/json"

	"github.com/dgrijalva/jwt-go"
)

// TokenClaims are claims found in JWT token distributed by authorization server
type TokenClaims struct {
	jwt.StandardClaims
	Audience    audiences `json:"aud,omitempty"`
	Permissions []string  `json:"permissions,omitempty"`
	Scope       string    `json:"scope,omitempty"`
}

type audiences []string

// UnmarshalJSON unmarshalls JSON string into array of strings and leaves array of strings as array of strings
func (a *audiences) UnmarshalJSON(data []byte) error {
	if len(data) == 0 {
		return nil
	}

	firstChar := data[0]

	if firstChar != '[' {
		var audience string
		err := json.Unmarshal(data, &audience)
		if err != nil {
			return err
		}

		*a = []string{audience}
		return nil
	}

	var arrayOfAudiences []string
	err := json.Unmarshal(data, &arrayOfAudiences)
	if err != nil {
		return err
	}

	*a = arrayOfAudiences

	return nil
}
