package auth

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Permissions defines which permissions are required
type Permissions struct {
	// All contains permissions which all need to be present
	All []string
}

// NewPermissionsMiddleware creates middleware which checks if user has required permissions
func NewPermissionsMiddleware(config Permissions) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			user := GetRequestUser(c)
			allow := true

			if user != nil {
				allow = user.HasAllPermissions(config.All)
			} else {
				allow = false
			}

			if allow {
				return next(c)
			}

			return &echo.HTTPError{
				Code:    http.StatusForbidden,
				Message: "user does not have access to this resource",
			}
		}
	}
}
