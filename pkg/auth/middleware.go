package auth

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
)

// MiddlewareConfig defines the configuration for auth.Middleware
type MiddlewareConfig struct {
	// JWKSEndpoint defines the endpoint that will be used to retrieve JSON Web Key Set.
	JWKSEndpoint string

	// KeySetProvider provides jwk.Set based on provided JWKSEndpoint
	// Optional. Default value is HttpKeySetProvider
	KeySetProvider jwksmiddleware.KeySetProvider
}

// NewMiddleware creates middleware which verifies tokens associated with request
func NewMiddleware(config MiddlewareConfig) echo.MiddlewareFunc {
	jwksConfig := jwksmiddleware.Config{
		JWKSEndpoint:   config.JWKSEndpoint,
		Claims:         &TokenClaims{},
		KeySetProvider: config.KeySetProvider,
	}

	jwks := jwksmiddleware.WithConfig(jwksConfig)

	return jwks
}

// GetRequestUser returns either a user associated with request or nil when there is no user information
func GetRequestUser(c echo.Context) *RequestUser {
	token := jwksmiddleware.GetRequestJWT(c)
	var claims *TokenClaims
	if token != nil && token.Claims != nil {
		claims = token.Claims.(*TokenClaims)
	}

	if claims == nil {
		return nil
	}

	return &RequestUser{
		ID:          claims.Subject,
		Permissions: claims.Permissions,
		Scope:       claims.Scope,
	}
}
