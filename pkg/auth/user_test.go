package auth_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
)

func TestRequestUser_HasPermission(t *testing.T) {
	u := auth.RequestUser{
		Permissions: []string{"a", "b"},
	}
	testCases := []struct {
		name       string
		user       auth.RequestUser
		permission string
		expected   bool
	}{
		{
			name:       "user has permission",
			permission: "a",
			expected:   true,
		},
		{
			name:       "user doesn't have permission",
			permission: "c",
			expected:   false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			permission := testCase.permission
			expected := testCase.expected

			// When
			result := u.HasPermission(permission)

			// Then
			assert.Equal(t, result, expected)
		})
	}
}

func TestRequestUser_HasAllPermissions(t *testing.T) {
	u := auth.RequestUser{
		Permissions: []string{"a", "b", "c"},
	}
	testCases := []struct {
		name        string
		user        auth.RequestUser
		permissions []string
		expected    bool
	}{
		{
			name:        "user has all required permissions",
			permissions: []string{"a", "b"},
			expected:    true,
		},
		{
			name:        "user has some required permissions",
			permissions: []string{"a", "d"},
			expected:    false,
		},
		{
			name:        "user doesn't have any required permissions",
			permissions: []string{"d", "e"},
			expected:    false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			permissions := testCase.permissions
			expected := testCase.expected

			// When
			result := u.HasAllPermissions(permissions)

			// Then
			assert.Equal(t, result, expected)
		})
	}
}
