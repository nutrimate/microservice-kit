package jwksmiddleware_test

import (
	"github.com/lestrrat-go/jwx/jwk"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
	"gitlab.com/nutrimate/microservice-kit/tests/jwkstest"
)

func TestJWKSWithConfig(t *testing.T) {
	jwksPath, err := filepath.Abs("../../tests/jwkstest/jwks.json")
	assert.NoError(t, err)

	config := jwksmiddleware.Config{
		JWKSEndpoint: jwksPath,
		KeySetProvider: func(uri string) (jwk.Set, error) {
			return jwk.ReadFile(uri)
		},
	}

	sourceToken, err := jwkstest.CreateToken()
	assert.NoError(t, err)

	privateKeyPath := "../../tests/jwkstest/key"
	tokenWithValidSignature, err := jwkstest.SignTokenWithDefaultKey(privateKeyPath, sourceToken)
	assert.NoError(t, err)

	tokenWithInvalidSignature, err := jwkstest.SignTokenWithRandomKey(sourceToken)
	assert.NoError(t, err)

	tests := []struct {
		name           string
		token          string
		expectedStatus int
	}{
		{
			name:           "allows correct sourceToken",
			token:          string(tokenWithValidSignature),
			expectedStatus: http.StatusOK,
		},
		{
			name:           "rejects incorrect sourceToken",
			token:          string(tokenWithInvalidSignature),
			expectedStatus: http.StatusUnauthorized,
		},
	}

	e := echo.New()
	e.Use(jwksmiddleware.WithConfig(config))
	e.GET("/", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			// Given
			token := testCase.token
			expectedStatus := testCase.expectedStatus

			req := httptest.NewRequest("GET", "/", nil)
			req.Header.Set("Authorization", "Bearer "+token)
			rec := httptest.NewRecorder()

			// When
			e.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, expectedStatus, rec.Code)
		})
	}
}
