package jwksmiddleware

import (
	"context"
	"errors"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	echoMiddleware "github.com/labstack/echo/v4/middleware"
	"github.com/lestrrat-go/jwx/jwk"
)

// JWTContextKey is a context key under which user information from the token is stored.
const JWTContextKey = "gitlab.com/nutrimate/microservice-kit/pkg/auth/JWT"

// KeySetProvider represents function that obtains jwk.Set from given location
type KeySetProvider = func(uri string) (jwk.Set, error)

// Config defines the config for Config middleware.
type Config struct {
	// JWKSEndpoint defines the endpoint that will be used to retrieve JSON Web Key Set.
	JWKSEndpoint string

	// Signing method, used to check token signing method.
	// Optional. Default value RS256.
	SigningMethod string

	// Claims are extendable claims data defining token content.
	// Optional. Default value jwt.Claims
	Claims jwt.Claims

	// Skipper defines a function to skip middleware.
	Skipper echoMiddleware.Skipper

	// BeforeFunc defines a function which is executed just before the middleware.
	BeforeFunc echoMiddleware.BeforeFunc

	// SuccessHandler defines a function which is executed for a valid token.
	SuccessHandler echoMiddleware.JWTSuccessHandler

	// ErrorHandler defines a function which is executed for an invalid token.
	// It may be used to define a custom JWT error.
	ErrorHandler echoMiddleware.JWTErrorHandler

	// ErrorHandlerWithContext is almost identical to ErrorHandler, but it's passed the current context.
	ErrorHandlerWithContext echoMiddleware.JWTErrorHandlerWithContext

	// KeySetProvider provides jwk.Set based on provided JWKSEndpoint
	// Optional. Default value is HttpKeySetProvider
	KeySetProvider KeySetProvider
}

// WithConfig returns an echo.JWT auth middleware that fetches signing keys.
func WithConfig(config Config) echo.MiddlewareFunc {
	if config.JWKSEndpoint == "" {
		panic("echo: jwks middleware requires jwks endpoint")
	}
	if config.SigningMethod == "" {
		config.SigningMethod = "RS256"
	}

	if config.KeySetProvider == nil {
		config.KeySetProvider = httpKeySetProvider
	}

	signingKeys, err := getSigningKeys(config.KeySetProvider, config.JWKSEndpoint)

	if err != nil {
		message := fmt.Sprintf("failed to prepare signing keys: %v", err)
		panic(message)
	}

	return echoMiddleware.JWTWithConfig(echoMiddleware.JWTConfig{
		Skipper:                 config.Skipper,
		BeforeFunc:              config.BeforeFunc,
		SuccessHandler:          config.SuccessHandler,
		ErrorHandler:            config.ErrorHandler,
		ErrorHandlerWithContext: config.ErrorHandlerWithContext,
		SigningKeys:             signingKeys,
		SigningMethod:           config.SigningMethod,
		ContextKey:              JWTContextKey,
		Claims:                  config.Claims,
	})
}

func httpKeySetProvider(uri string) (jwk.Set, error) {
	ctx := context.Background()

	return jwk.Fetch(ctx, uri)
}

func getSigningKeys(fetchKeys func(uri string) (jwk.Set, error), uri string) (map[string]interface{}, error) {
	result, err := fetchKeys(uri)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch keys: %w", err)
	}
	if result == nil {
		return nil, errors.New("failed to fetch keys: keys are empty")
	}

	numberOfKeys := result.Len()
	keySet := make(map[string]interface{}, numberOfKeys)

	for i := 0; i < numberOfKeys; i++ {
		key, hasKey := result.Get(i)
		if !hasKey {
			continue
		}
		kid := key.KeyID()
		var raw interface{}
		err = key.Raw(&raw)
		if err != nil {
			return nil, fmt.Errorf("failed to create raw key for kid=%s: %w", kid, err)
		}

		keySet[kid] = raw
	}

	return keySet, nil
}

// GetRequestJWT returns *jwt.Token set by JWT middleware in context
func GetRequestJWT(c echo.Context) *jwt.Token {
	token, ok := c.Get(JWTContextKey).(*jwt.Token)
	if !ok {
		return nil
	}

	return token
}
