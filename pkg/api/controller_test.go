package api_test

import (
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/microservice-kit/pkg/api"
)

type MockController struct {
	mock.Mock
}

func (c *MockController) Register(e *echo.Echo) {
	c.Mock.Called(e)
}

func TestRegisterControllers(t *testing.T) {
	t.Run("should register controllers", func(t *testing.T) {
		e := api.NewBare()
		controllerA := &MockController{}
		controllerB := &MockController{}
		controllerA.On("Register", e).Return()
		controllerB.On("Register", e).Return()

		api.RegisterControllers(e, []api.Controller{
			controllerA,
			controllerB,
		})

		controllerA.AssertCalled(t, "Register", e)
		controllerB.AssertCalled(t, "Register", e)
	})
}
