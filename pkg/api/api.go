package api

import (
	"github.com/getsentry/sentry-go"
	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/nutrimate/microservice-kit/pkg/api/validator"
)

// NewBare creates minimal echo instance that can be used for tests
func NewBare() *echo.Echo {
	e := echo.New()
	e.Validator = validator.New()
	e.Binder = validator.ValidationBinder{}

	return e
}

// NewWithMiddleware creates echo instance with NewBare and adds middleware
func NewWithMiddleware() *echo.Echo {
	e := NewBare()
	e.HideBanner = true
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		e.Logger.Error(err)
		e.DefaultHTTPErrorHandler(err, c)

		httpError, ok := err.(*echo.HTTPError)
		if (ok && httpError.Code >= 500) || !ok {
			sentry.CaptureException(err)
		}
	}

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Skipper: func(context echo.Context) bool {
			path := context.Path()

			return path == "/health"
		},
		Format: "[${time_rfc3339}] ${id} ${method} ${uri} ${status} ${latency_human}\n",
	}))
	e.Use(sentryecho.New(sentryecho.Options{}))
	e.Use(middleware.RequestID())
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Recover())

	return e
}
