package api

import (
	"github.com/labstack/echo/v4"
)

// Controller represents generic controller which can handle routes
type Controller interface {
	Register(e *echo.Echo)
}

// RegisterControllers registers calls Register method for all controllers with given server
func RegisterControllers(e *echo.Echo, controllers []Controller) {
	for _, c := range controllers {
		c.Register(e)
	}
}
