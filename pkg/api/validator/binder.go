package validator

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// ValidationBinder is a binder that automatically performs validation
type ValidationBinder struct {
	binder echo.DefaultBinder
}

// Bind calls DefaultBinder.Bind and performs validation
func (b ValidationBinder) Bind(i interface{}, c echo.Context) error {
	err := b.binder.Bind(i, c)
	if err != nil {
		return err
	}

	err = c.Validate(i)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	return nil
}
