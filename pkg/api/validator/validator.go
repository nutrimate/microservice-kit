package validator

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

type echoValidator struct {
	validator *validator.Validate
}

// Validate validates the struct using go-playground/validator
func (v *echoValidator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}

// New creates a new echo.Validator using go-playground/validator
func New() echo.Validator {
	return &echoValidator{
		validator: validator.New(),
	}
}
