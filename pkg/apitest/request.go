package apitest

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
)

// NewRequest creates new http request with optional JSON body
func NewRequest(method string, target string, body []byte) *http.Request {
	req := httptest.NewRequest(method, target, bytes.NewBuffer(body))
	if body != nil {
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	}

	return req
}

// NewAuthRequest calls NewRequest and sets up authorization header for MockAuthMiddleware
func NewAuthRequest(method string, target string, body []byte) *http.Request {
	req := NewRequest(method, target, body)
	req.Header.Set("Authorization", mockAuthMiddlewareAuthorizationHeader)

	return req
}

// NewAuthRequestWithUser calls NewAuthRequest and sets up user data for MockAuthMiddleware
func NewAuthRequestWithUser(user auth.RequestUser, method string, target string, body []byte) *http.Request {
	req := NewAuthRequest(method, target, body)
	req.Header.Set(mockAuthMiddlewareTokenSubjectHeader, user.ID)
	req.Header.Set(mockAuthMiddlewareTokenScopeHeader, user.Scope)
	req.Header.Set(mockAuthMiddlewareTokenPermissionsHeader, strings.Join(user.Permissions, ","))

	return req
}
