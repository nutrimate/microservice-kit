package apitest

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/microservice-kit/pkg/api"
)

// SetupTestAPIServer creates an API server with provided controller
func SetupTestAPIServer(controller api.Controller) *echo.Echo {
	server := api.NewBare()
	controller.Register(server)

	return server
}
