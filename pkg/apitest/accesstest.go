package apitest

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
)

// AccessTest contains utilities for testing route access
type AccessTest interface {
	ShouldUseAuthMiddleware(t *testing.T, method string, path string)
	ShouldCheckPermissions(t *testing.T, method string, path string)
}

type accessTest struct {
	serverProvider func() *echo.Echo
}

// ShouldUseAuthMiddleware checks if controller uses middleware which prevents unauthorized access
func (at accessTest) ShouldUseAuthMiddleware(t *testing.T, method string, path string) {
	t.Run("should not allow unauthorized access", func(t *testing.T) {
		// Given
		server := at.serverProvider()
		req := NewRequest(method, path, nil)
		rec := httptest.NewRecorder()

		// When
		server.ServeHTTP(rec, req)

		// Then
		assert.Equal(t, http.StatusUnauthorized, rec.Code)
	})
}

// ShouldCheckPermissions checks if controller checks permissions by testing as authenticated user without permissions
func (at accessTest) ShouldCheckPermissions(t *testing.T, method string, path string) {
	t.Run("should not allow access without permissions", func(t *testing.T) {
		// Given
		server := at.serverProvider()
		req := NewAuthRequestWithUser(auth.RequestUser{}, method, path, nil)
		rec := httptest.NewRecorder()

		// When
		server.ServeHTTP(rec, req)

		// Then
		assert.Equal(t, http.StatusForbidden, rec.Code)
	})
}

// NewAccessTest returns struct that provides tests for checking controller access
func NewAccessTest(serverProvider func() *echo.Echo) AccessTest {
	return accessTest{
		serverProvider: serverProvider,
	}
}
