package apitest

import (
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/microservice-kit/pkg/auth"
	"gitlab.com/nutrimate/microservice-kit/pkg/jwksmiddleware"
)

const mockAuthMiddlewareAuthorizationHeader = "Bearer some.mock.token"
const mockAuthMiddlewareTokenSubjectHeader = "X-Mock-Token-Subject"
const mockAuthMiddlewareTokenScopeHeader = "X-Mock-Token-Scope"
const mockAuthMiddlewareTokenPermissionsHeader = "X-Mock-Token-Permissions"

// MockAuthMiddleware creates a mock version of auth.Middleware for use in tests
func MockAuthMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			authorization := c.Request().Header.Get("Authorization")

			if authorization != mockAuthMiddlewareAuthorizationHeader {
				return echo.NewHTTPError(http.StatusUnauthorized)
			}

			subjectHeader := c.Request().Header.Get(mockAuthMiddlewareTokenSubjectHeader)
			scopeHeader := c.Request().Header.Get(mockAuthMiddlewareTokenScopeHeader)
			permissionsHeader := c.Request().Header.Get(mockAuthMiddlewareTokenPermissionsHeader)

			var permissions []string
			if permissionsHeader != "" {
				permissions = strings.Split(permissionsHeader, ",")
			}

			token := jwt.New(jwt.SigningMethodNone)
			token.Claims = &auth.TokenClaims{
				Audience:    []string{"test"},
				Scope:       scopeHeader,
				Permissions: permissions,
				StandardClaims: jwt.StandardClaims{
					Subject: subjectHeader,
				},
			}

			c.Set(jwksmiddleware.JWTContextKey, token)

			return next(c)
		}
	}
}
