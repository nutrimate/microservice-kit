package jwkstest

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jws"
	"github.com/lestrrat-go/jwx/jwt"
)

// CreateToken creates jwt.Token that expires in an hour
func CreateToken() (jwt.Token, error) {
	token := jwt.New()
	err := token.Set(jwt.IssuedAtKey, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	err = token.Set(jwt.ExpirationKey, time.Now().Add(1*time.Hour).Unix())
	if err != nil {
		return nil, err
	}

	return token, nil
}

const defaultKeyID = "CSU4peApjg71XFEHP16vbl0B7CnQf3Z1Q5-DAlfwL9Q"

// SignTokenWithDefaultKey signs the token with private key that was used to generate jwks.json file
func SignTokenWithDefaultKey(keyPath string, token jwt.Token) ([]byte, error) {
	privateKey, err := loadRSAPrivateKeyFromFile(keyPath)
	if err != nil {
		return nil, fmt.Errorf("failed to load key from file: %w", err)
	}

	headers := jws.NewHeaders()
	err = headers.Set(jws.KeyIDKey, defaultKeyID)
	if err != nil {
		return nil, fmt.Errorf("failed to set key ID: %w", err)
	}

	return jwt.Sign(token, jwa.RS256, privateKey, jwt.WithHeaders(headers))
}

// SignTokenWithRandomKey signs the token with newly generated privat key
func SignTokenWithRandomKey(token jwt.Token) ([]byte, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, fmt.Errorf("failed to generate key: %w", err)
	}

	headers := jws.NewHeaders()
	err = headers.Set(jws.KeyIDKey, "random-key")
	if err != nil {
		return nil, fmt.Errorf("failed to set key ID: %w", err)
	}

	return jwt.Sign(token, jwa.RS256, privateKey, jwt.WithHeaders(headers))
}
