module gitlab.com/nutrimate/microservice-kit

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.9.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/labstack/echo/v4 v4.2.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lestrrat-go/jwx v1.1.2
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210218155724-8ebf48af031b // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
